
#ifndef VARIABLES_H_INCLUDED
#define VARIABLES_H_INCLUDED

#include "QTRSensors/QTRSensors.h"

// piny

#define SENSOR_ARRAY_1_PIN A0
#define SENSOR_ARRAY_2_PIN A1
#define SENSOR_ARRAY_3_PIN A2
#define SENSOR_ARRAY_4_PIN A3
#define SENSOR_ARRAY_EMMITER_CONTROL_PIN 0
#define SOUND_PIN 1
#define MOTOR_1_PWM_PIN 10
#define MOTOR_2_PWM_PIN 9
#define MOTOR_1_DIRECTION_1 8
#define MOTOR_1_DIRECTION_2 7
#define MOTOR_2_DIRECTION_1 4
#define MOTOR_2_DIRECTION_2 2
#define RED_LED_PIN 6
#define BLUE_LED_PIN 5
#define GREEN_LED_PIN 3
#define ACUMULATOR_CHECK_PIN A6
#define SIDE_SENSOR_LEFT  0//also digital
#define SIDE_SENSOR_RIGHT 13//digital

//zmienne globalne

//=====================================================interakcja================

bool isMusicPlaying = false;

int redLedState = LOW;
int blueLedState = LOW;
int greenLedState = LOW;

//===========================================================motor control======================================================
/*const uint8_t LeftEngineDirection = 1; //engine żal mi siebie xdd
const uint8_t RightEngineDirection = 2;
const uint8_t LeftEnginePWM = 5;
const uint8_t RightEnginePWM = 6;
*/
/*
const int readingColoursPWM = 60;
const int slowPWM = 40;
const int verySlowPWM = 20;
const int fastPWM = 80;
const int veryFastPWM = 100;
*/
//obecna predkosc, na razie wyrazona w PWMach (konkretnie wypelnienie)
//int currentPWM = readingColoursPWM; // musi być w zakresie, bodajże do 255 albo 254

int velocityThroughout = 0;
const int velocityMinNumber = -2;
const int velocityMaxNumber = 2;
const int velocityOnColours = 0;

enum motor { Left = 0, Right = 1 };


//==============================================================action=========================================================

const unsigned long TurningBackInterval = 1000;
const unsigned long TurningAroundInterval = 2000;
const unsigned long TurningInterval = 250;

enum actionType {
    wPrawo,
    wLewo,
    zawroc,
    postoj,
    muzyka,
    zagadka,
    obrotLewo,
    obrotPrawo,
    wolniej,
    sporoWolniej,
    szybciej,
    sporoSzybciej
};

//============================================================turning============================================================
uint8_t turnSelection = 0; // 0 - pusto, 1 - LEFT, 2 - RIGHT
uint8_t turnPossibility[2] = { 0, 0 };
//                            L, P
int leftTurningSensorData = 0;
int rightTurningSensorData = 0;

const long granicaTurn = 950;

bool offTheLineAfterTurn = true;

//=========================================================data from file==================================================
char line[25];
int ifClosed = 1;
FILE* filePointer;
bool ifOpened = false;
int ifInFirst = 0;
char* ptr = line;
std::vector<double> lineReadingResult;

unsigned long startingMillis;
unsigned long currentMillis;

bool stuffIsSet = false;
bool stuffIsSet2 = false;
bool stuffIsSet3 = false;
//=======================================================colour reading=====================================================
char colour = '-';
char previousColour = '-';
char colourArray[3] = { '-','-','-' }; // - tablica niezapisana
char colourArrayTemp[3] = { '-','-','-' };
enum colourCheckingType {
    akcja,
    zaMalo,
    zaDuzo,
    nic
};
colourCheckingType colourCheckingResult = nic;
int colourSensorData = 0;
//=====================================================line following======================================================
int threshold = 0;
int previousError = 0;
int error = 0;
int madeError = 0;

const int Kp = 1;
const int Kd = 0.04;

QTRSensorsAnalog sensorArray((unsigned char[]) {0,1,2,3}, 4, 4, SENSOR_ARRAY_1_PIN);
unsigned int sensorValues[4];
int sensorBoolResults[4];
int weights[4] = {-15, -5, 5, 15};

const long granica = 860; //powy¿ej tej wartoœci czujnik widzi liniê
int leftSensorData = 0;
int rightSensorData = 0;

int lastError = 0;
//===========================================================================================================================



#endif // VARIABLES_H_INCLUDED
