#ifndef COLOURREADING_H_INCLUDED
#define COLOURREADING_H_INCLUDED

#define println Serial.println
#define print Serial.print

#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"

template<int LEDPIN>
void handleLed( bool onOff, int& ledState);

void printcolourArray() {
    print(colourArray[0]);print(" ");print(colourArray[1]);print(" ");println(colourArray[2]);
}

void cleancolourArrayTemp() {
    colourArrayTemp[2] = '-';
    colourArrayTemp[1] = '-';
    colourArrayTemp[0] = '-';
}

void cleancolourArray() {
    colourArray[2] = '-';
    colourArray[1] = '-';
    colourArray[0] = '-';
}

char colourFromSensorTranslate(int colour) {
    switch (colour) {
    case 1:
        return 'R';
    case 2:
        return 'G';
    case 3:
        return 'B';
    default:
        return 'C';
    }
}

// modyfikuje zmienna colour, previousColour, colourArrayTemp, Colour Array i currentPWM
colourCheckingType colourChecking() {
    colourCheckingType temp = nic;
    colour = colourFromSensorTranslate(colourSensorData);
  //  byte_8 Changed = true;
    if (colour != previousColour) {
        print("colourReading.h ");print(currentMillis); print("| jest zmiana |");print(colour);print("\n");
        if (colour != 'C') { 
            //predkosc na kolorach
            velocityHandlerColours();
            print(colour);print(" ");
            if (colour == 'R') {
                handleLed<GREEN_LED_PIN>(false, greenLedState);
                handleLed<BLUE_LED_PIN>(false, blueLedState);
                handleLed<RED_LED_PIN>(true, redLedState);
            } else if (colour == 'G') {
                handleLed<RED_LED_PIN>(false, redLedState);
                handleLed<BLUE_LED_PIN>(false, blueLedState);
                handleLed<GREEN_LED_PIN>(true, greenLedState);
            } else if (colour == 'B') {
                handleLed<RED_LED_PIN>(false, redLedState);
                handleLed<GREEN_LED_PIN>(false, greenLedState);
                handleLed<BLUE_LED_PIN>(true, blueLedState);
            }

            if(colourArrayTemp[2] != '-') { // tablica jest juz pelna przed dodaniem - za dluga sekwencja!
                cleancolourArrayTemp();
                temp = zaDuzo;
            } else { // jesli nie, dodaj do tablicy, na razie sekwencjaa nie za dluga
                colourArrayTemp[2] = colourArrayTemp[1];
                colourArrayTemp[1] = colourArrayTemp[0];
                colourArrayTemp[0] = colour;
                print("colourReading.h ");print(currentMillis);print(" Dodano do tablicy "); print("\n");
            }
        //jezeli kolor jest czarny
        } else {
            handleLed<RED_LED_PIN>(false, redLedState);
            handleLed<GREEN_LED_PIN>(false, greenLedState);
            handleLed<BLUE_LED_PIN>(false, blueLedState);
            //jezeli tablica jest pelna
            if (colourArrayTemp[2] != '-' && colourArrayTemp[1] != '-' && colourArrayTemp[0] != '-') {
                colourArray[0] = colourArrayTemp[0];
                colourArray[1] = colourArrayTemp[1];
                colourArray[2] = colourArrayTemp[2];
                cleancolourArrayTemp();
                temp = akcja; // jest ok
                print("colourReading.h ");print(currentMillis); print(" zwrocilo akcja"); print("\n");
            //jezeli tablica nie jest pelna - zawsze wtedy ostatnia komorka jest niezapelniona ale tablica nie jest te� pusta - czyli pierwsza komorka jest zapisana
            } else if (colourArrayTemp[2] == '-' && colourArrayTemp[0] != '-') {
                cleancolourArrayTemp();
                temp = zaMalo; //sekwencja skonczyla sie przedwczesnie - za malo kolorow
            }
        }
    }
    previousColour = colour;
    return temp;
}

uint8_t comparingWithArray(char fC, char sC, char tC) {
    if (colourArray[0] == fC && colourArray[1] == sC && colourArray[2] == tC)
        return 1;
    else
        return 0;
}


#endif // COLOURREADING_H_INCLUDED
