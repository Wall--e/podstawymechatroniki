#ifndef ACTIONS_H_INCLUDED
#define ACTIONS_H_INCLUDED

#define println Serial.println
#define print Serial.print

#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"


//=========== funkcje akcyjne=============================//
actionType whatAction() { //odczytuje macierz colourArray i sprawdza jaka akcje wykonac
    /*1*/   if(comparingWithArray('R','B','R')) {//na nastepnym skrec w prawo
                return wPrawo;
    /*2*/   } else if(comparingWithArray('R','G','R')) {//zawroc
                return zawroc;
    /*3*/   } else if(comparingWithArray('B','R','B')) {//na nastepnym skrec w lewo 3 1 3
                return wLewo;
    /*3*/   } else if(comparingWithArray('B','G','B')) {
                return postoj;
    /*4*/   } else if(comparingWithArray('G','R','G')) {
                return muzyka;
    /*5*/   } else if(comparingWithArray('G','B','G')) {
                return zagadka;
    /*6*/   } else if(comparingWithArray('R','B','G')) {
                return obrotLewo;
    /*7*/   } else if(comparingWithArray('R','G','B')) {
                return wolniej;
    /*8*/   } else if(comparingWithArray('G','B','R')) {
                return obrotPrawo;
    /*9*/   } else if(comparingWithArray('G','R','B')) {
                return sporoSzybciej;
    /*10*/  } else if(comparingWithArray('B','R','G')) {
                return sporoWolniej;
    /*11*/  } else if(comparingWithArray('B','G','R')) {
                return szybciej;
    /*12*/  }
}

void muzykaFunc() {
    isMusicPlaying ? isMusicPlaying = false : isMusicPlaying = true;
    print("actions.h Czy gra muzyka?: ");print(isMusicPlaying);print("\n");
}

void zagadkaFunc() {
    println("actions.h zagadka");
}

void obrotLewoFunc() {
    unsigned long actionStartTime = millis() - startingMillis;
    print("Zaczalem obracac sie w lewo: "); println(actionStartTime);
    unsigned long currentMillisTemp;
    do {
        currentMillisTemp = millis() - actionStartTime - startingMillis;
    } while (currentMillisTemp < TurningAroundInterval);
    print("I skonczylem: "); println(currentMillisTemp + actionStartTime);
}

void obrotPrawoFunc() {
    unsigned long actionStartTime = millis() - startingMillis;
    print("Zaczalem obracac sie w prawo: "); println(actionStartTime);
    unsigned long currentMillisTemp;
    do {
        currentMillisTemp = millis() - actionStartTime - startingMillis;
    } while (currentMillisTemp < TurningAroundInterval);
    print("I skonczylem: "); println(currentMillisTemp + actionStartTime);
}

void zawrocFunc() {
    unsigned long actionStartTime = millis() - startingMillis;
    print("Zaczalem zawracac: "); println(actionStartTime);
    unsigned long currentMillisTemp;
    do {
        currentMillisTemp = millis() - actionStartTime - startingMillis;
    //println(currentMillisTemp);
    } while (currentMillisTemp < TurningBackInterval);
    print("I skonczylem: "); println(currentMillisTemp + actionStartTime);
}

void wPrawoFunc() {  //modyfikuje turnSelection
    turnSelection = RIGHT;
    print("actions.h Postanawiam ze skrecac bedziemy w prawo\n");
}

void wLewoFunc() { //modyfikuje turnSelection
    turnSelection = LEFT;
    print("actions.h Postanawiam ze skrecac bedziemy w lewo\n");
}

void postojFunc() {
    unsigned long actionStartTime = millis() - startingMillis;
    print("Stanalem: "); println(actionStartTime);
    unsigned long currentMillisTemp;
    do {
        currentMillisTemp = millis() - actionStartTime - startingMillis;
    //println(currentMillisTemp);
    } while (currentMillisTemp < TurningBackInterval);
    print("I skonczylem: "); println(currentMillisTemp + actionStartTime);
}

void szybciejFunc() {
    println("actions.h jade szybciej");
    velocityHandler(1);
}

void wolniejFunc() {
    println("actions.h jade wolniej");
    velocityHandler(-1);
}

void sporoSzybciejFunc() {
    println("actions.h jade sporo szybciej");
    velocityHandler(2);
}

void sporoWolniejFunc() {
    println("actions.h jade sporo wolniej");
    velocityHandler(-2);
}
template<int LEDPIN>
void handleLed(bool onOff, int& ledState) {
    if (onOff) { //chce wlaczyc
        if (ledState == LOW) {
            ledState = HIGH;
            print("handleLed(): on ");println(LEDPIN);
         //   digitalWrite(LEDPIN, ledState);
        }
    }
    else { //chce wylaczyc
        if (ledState != LOW) {
            ledState = LOW;
            print("handleLed(): off ");println(LEDPIN);
           // digitalWrite(LEDPIN, ledState);
        }
    }
    /*
    switch (LEDPIN) {
    case RED_LED_PIN:
        if (onOff) { //chce wlaczyc
            if (redLedState == LOW) {
                redLedState = HIGH;
                print("handleLed(): R on ");
                digitalWrite(LEDPIN, redLedState);
            }
        } else { //chce wylaczyc
            if (redLedState != LOW) {
                redLedState = LOW;
                print("handleLed(): R off");
                digitalWrite(LEDPIN, redLedState);
            }
        }
        break;
    case BLUE_LED_PIN:
        if (onOff) {
            if (blueLedState == LOW) {
                blueLedState = HIGH;
                print("handleLed(): B on");
              //  digitalWrite(LEDPIN, blueLedState);
            }
        } else {
            if (blueLedState != LOW) {
                blueLedState = LOW;
                print("handleLed(): B off");
              //  digitalWrite(LEDPIN, blueLedState);
            }
        }
        break;
    case GREEN_LED_PIN:
        if (onOff) {
            if (greenLedState == LOW) {
                greenLedState = HIGH;
                print("handleLed(): G on");
               // digitalWrite(LEDPIN, greenLedState);
            }
        } else {
            if (greenLedState != LOW) {
                greenLedState = LOW;
                print("handleLed(): G off");
               // digitalWrite(LEDPIN, greenLedState);
            }
        }
        break;
    }
    */
}


#endif // ACTIONS_H_INCLUDED
