#ifndef TURNING_H_INCLUDED
#define TURNING_H_INCLUDED

#define println Serial.println
#define print Serial.print

#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"

#define LEFT 1
#define RIGHT 2

//============= funkcje skrecajace =======================//

//generuje komende losowego skrecania w LEFT albo RIGHT: 1 - LEFT, 2 - RIGHT
uint8_t leftOrRight() {
    int i = rand() / (RAND_MAX + 1.0) * 100.0;
    if (i < 50) {
        return LEFT;
    } else {return RIGHT;}
}

// jesli czujnik skretu widzi linie i robot zjechal juz z linii po skrecie ustawia turnPossibilities
void checkForTurn() {
    if (offTheLineAfterTurn) { //jesli zjechal juz z linii po skrecie
            //print("turning.h ");print(currentMillis); print(" lewy skret: "); print(leftTurningSensorData);
            //print(" prawy skret: "); print(rightTurningSensorData);print("\n");
        if (leftTurningSensorData > granicaTurn) { //mozna skrecac w lewo
            turnPossibility[0] = 1;
            //  print("turning.h ");print(currentMillis); print(" "); print("Zauwazony skret w lewo");print("\n");
        }
            //mozna skrecac w prawo
        if (rightTurningSensorData > granicaTurn) { //mozna skrecac w prawo
            turnPossibility[1] = 1;
            //print("turning.h ");print(currentMillis); print(" "); print("Zauwazony skret w prawo");print("\n");
        }
    } else {//jesli nie zjechal z linii
        if (rightTurningSensorData <= granicaTurn && leftTurningSensorData <= granicaTurn) {// nie widac linii
            offTheLineAfterTurn = true; // to znaczy ze juz zjechal z linii
        }
        // jesli ktoras widac to nic nie rob bo to nas obecnie nie obchodzi, a turnPossibility jest wciaz wyzerowane od ostatniego skretu
    }
}

void turningLeft() {
    unsigned long actionStartTime = millis() - startingMillis;
    print("Skrecam w lewo: "); println(actionStartTime);
    unsigned long currentMillisTemp;
    do {
        currentMillisTemp = millis() - actionStartTime - startingMillis;
        //println(currentMillisTemp);
    } while (currentMillisTemp < TurningInterval);
    print("I skonczylem: "); println(currentMillisTemp + actionStartTime);

    offTheLineAfterTurn = false; //!!!!!!!!!!!!!
}

void turningRight() {
    unsigned long actionStartTime = millis() - startingMillis;
    print("Skrecam w prawo: "); println(actionStartTime);
    unsigned long currentMillisTemp;

    do {
        currentMillisTemp = millis() - actionStartTime - startingMillis;
        //println(currentMillisTemp);
    } while (currentMillisTemp < TurningInterval);
    print("I skonczylem: "); println(currentMillisTemp + actionStartTime);

    offTheLineAfterTurn = false; //!!!!!!!!!!!!!!!!!
}

// modyfikuje turnPossibility (zerowanie na koncu) ora turnSelection(rowniez zerowanie na koncu)
void turning(){

    //jesli jest mozliwy skret
    if(turnPossibility[0] == 1 || turnPossibility[1] == 1) {
        uint8_t turnTemp = 0; // 1 -skrecamy w LEFT;  2 - skrecamy w RIGHT
        if (turnSelection == 0) { //nie ma instrukcji co do skretu
            if( turnPossibility[0] == 1 && turnPossibility[1] ==1) {//obydwie sciezki dostepne
                print("turning.h ");print("Losowanie");print("\n");
            turnTemp = leftOrRight();
            } else { // tylko jedna jest dostepna
                if(turnPossibility[0] == 1) { //jest mozliwosc skretu w LEFT
                    turnTemp = LEFT;
                } else {
                    turnTemp = RIGHT;
                }
            }
        } else { // jest instrukcja co do skretu
            if(turnPossibility[0] == 1 &&  turnSelection == 1 ||
               turnPossibility[1] == 1 &&  turnSelection == 2) { //jesli instrukcja sie zgadza z mozliwoscia skretu
                   turnTemp = turnSelection;
            } else { // instrukcja nie zgadza sie z mo�liwo�ci� skr�tu
                print("turning.h ");print("Niezgodna instrukcja: "); println(millis() - startingMillis); // pokazuje ze zepsules i jedzie gdzie mozna
                if (turnPossibility[0] == 1) { //mozna w lewo
                    turnTemp = LEFT;
                } else {
                    turnTemp = RIGHT;
                }
            }
        }
        // po skrecie nalezy wyzerowac turn Possibilities i turnSelection
       // zerowanie turn selection, zeby nie zostala w pamieci poprzednia instrukcja

    if (turnTemp == LEFT) {
        turningLeft();
        turnPossibility[0] = 0;
        turnPossibility[1] = 0;
        turnSelection = 0;
    } else if (turnTemp == RIGHT) {
      //  println("Right");
        turningRight();
        turnPossibility[0] = 0;
        turnPossibility[1] = 0;
        turnSelection = 0;
    }

    }
}


#endif // TURNING_H_INCLUDED
