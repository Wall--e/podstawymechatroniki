#ifndef DATAFROMFILE_H_INCLUDED
#define DATAFROMFILE_H_INCLUDED

#define println Serial.println
#define print Serial.print

#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"

#define READING_LINE_LENGTH 100
#define DELIMITER ' '
//#define COMMENT_CHARACTER '/'

char colourFromSensorTranslate(int colour);

void setStuff() { // ustawia zmienne turnSelection i turnPossibility
    if (currentMillis >= 2005 && !stuffIsSet) {
        turnPossibility[0] = 0;
        turnPossibility[1] = 1;
        stuffIsSet = true;
    }
    if (millis() - startingMillis >= 1600 && !stuffIsSet2) {
        turnPossibility[0] = 1;
        turnPossibility[1] = 1;
        stuffIsSet2 = true;
    }
    if (millis() - startingMillis >= 1300 && !stuffIsSet3) {
        turnPossibility[0] = 1;
        turnPossibility[1] = 0;
        stuffIsSet3 = true;
    }
}

std::vector<double> lineReading(char *line) { //otrzymujemy wska�nik do tablicy
	std::vector<double> tempVec;

	char temp = '/';
	int i = 0;
	int base = 0;
	double tempInt = 0;
	int counter = 0;
	bool theEnd = false;

	while (!theEnd){
		base += counter;
		i = base;
		tempInt = 0;
		counter = 0;
		//sprawdz jak dlugi jest nastepna sekwencja cyfr do spacji lub nowej linii, jest dlugi na counter-2 chyba
		do {
			temp = line[i];
			i++;
			counter++;
		} while (temp != DELIMITER && temp != '\n');
		//"przetlumacz" sekwencje charow na liczbe
		for (int i = base; i < base+ counter - 1 ; i++) {
			temp = line[i];
			if (temp != '0')
			tempInt +=  pow(10,counter-2-i+base)*((int)(temp)-48);
		}
		tempVec.push_back(tempInt);
		//jesli nastepny jest znak nowej linii wyjdz z petli, linia zostala przeczytana
		if (line[base+counter - 1] == '\n')
			theEnd = true;

	}
	return tempVec;
}

void printVector(std::vector<double> vec) {
	for (size_t i; i < vec.size(); i++) {
		print(i); print(" ");printf("%d",lineReadingResult.at(i)); print(" ");
	}
	print("\n");
}

void fileSetUp (){
	filePointer = fopen("testPredkosc.txt", "r");
	if (filePointer == NULL) {
		println("File failed to open");
	} else {
	 println("OK");
	 ifOpened = true;
	}
}

void setFromFile() {
  if (ifOpened) {
        if(ptr != NULL) {
           // print("nie null");
            if (ifInFirst == 0) {
                    ptr = fgets (line, READING_LINE_LENGTH , filePointer);
					ptr = fgets (line, READING_LINE_LENGTH , filePointer);
					lineReadingResult = lineReading(line); //przekazujemy adres do tablicy
					ifInFirst++;
				//dane zaladowane, sprawdzamy kiedy nastapi wczytany czas
				} else if (ifInFirst > 0 && lineReadingResult.at(0) <= currentMillis) {
				//	std::cout << "time: " << currentMillis << " | set values: ";
//============================ tu sie dzieje to co ma sie dziac w  czasie "zmiany" - pierwsza kolumna pliku z danymi==================

					//print(lineReadingResult.at(0)); print(" ");
					leftSensorData = (int)(lineReadingResult.at(1));
					//print(leftSensorData); print(" ");
					rightSensorData = (int)(lineReadingResult.at(2));
					//print(rightSensorData); print(" ");
					leftTurningSensorData = (int)(lineReadingResult.at(3));
					//print(leftTurningSensorData); print(" ");
					rightTurningSensorData = (int)(lineReadingResult.at(4));
					//print(rightTurningSensorData); print(" ");
					colourSensorData = (int)(lineReadingResult.at(5));
				//	print(colourFromSensorTranslate(colourSensorData)); println(" ");
				//	delay(10);

//====================================================================================================================================
					ptr = fgets(line, READING_LINE_LENGTH , filePointer);
					if (ptr != NULL) {
						//if(line[1] != COMMENT_CHARACTER)
							lineReadingResult = lineReading(line);
					//	else
						//	ptr = fgets(line, READING_LINE_LENGTH, filePointer);
					}
				}
        } else { // kiedy koniec pliku
            if (ifClosed != 0) {
                ifClosed = fclose(filePointer);
                ifOpened = false;
		//		printf("File closed.\n");
				delete ptr;
            }
        }
    }
}

#endif // DATAFROMFILE_H_INCLUDED
