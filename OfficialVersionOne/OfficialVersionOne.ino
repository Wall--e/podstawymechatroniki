//backup 02.05.2020

#include <Arduino.h>
#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"

#define println Serial.println
#define print Serial.print

void setup(){
	Serial.begin(9600);
	startingMillis = millis();

	srand(millis());

	//4 piny do sterowania silnikami
    pinMode(SOUND_PIN, OUTPUT);
    pinMode(MOTOR_1_DIRECTION_1, OUTPUT);
    pinMode(MOTOR_1_DIRECTION_2, OUTPUT);
    pinMode(MOTOR_1_PWM_PIN, OUTPUT);
    pinMode(MOTOR_2_DIRECTION_1, OUTPUT);
    pinMode(MOTOR_2_DIRECTION_2, OUTPUT);
    pinMode(MOTOR_2_PWM_PIN, OUTPUT);
    pinMode(RED_LED_PIN, OUTPUT);
    pinMode(BLUE_LED_PIN, OUTPUT);
    pinMode(GREEN_LED_PIN,OUTPUT);
    pinMode(SIDE_SENSOR_LEFT, INPUT);
    pinMode(SIDE_SENSOR_RIGHT, INPUT);

  //  sensorArrayCallibration();

    fileSetUp();

}

void loop(){
    currentMillis = millis() - startingMillis;
    setFromFile();
    checkForTurn(); // jesli pojawi sie zmiana na czujniku mozliwosci skretu ustawia turnPossibilities

 //   print(currentMillis); print(" "); print(turnPossibility[0]);print(" "); print(turnPossibility[1]);print("\n");
    colourCheckingResult = colourChecking();
    switch(colourCheckingResult) {
    case akcja:
  //      actionType whatActionHappensNow = whatAction();
  //      print("main.ino "); print(whatActionHappensNow);
      //  println(" <- action type");
        switch(whatAction()) {
            case wLewo:
                wLewoFunc();
             //    print(turnPossibility[0]);print(" "); print(turnPossibility[1]); print("\n");
                break;
            case wPrawo:
                wPrawoFunc();
                break;
            case postoj:
                postojFunc();
                break;
            case zawroc:
                zawrocFunc();
                break;
            case muzyka:
                muzykaFunc();
                break;
            case zagadka:
                zagadkaFunc();
                break;
            case obrotLewo:
                obrotLewoFunc();
                break;
            case obrotPrawo:
                obrotPrawoFunc();
                break;
            case wolniej:
                wolniejFunc();
                break;
            case sporoWolniej:
                sporoWolniejFunc();
                break;
            case sporoSzybciej:
                sporoSzybciejFunc();
                break;
            case szybciej:
                szybciejFunc();
                break;
            //wszystkie mozliwosci wykorzystane
            default:
                println("main.ino It shouldn't have happened");
         }
        break;
    case zaDuzo:
        //print(currentMillis); print(" "); printcolourArray(); print(" ");
        println("main.ino Wrong sequence - too much colours");
        break;
    case zaMalo:
        //print(currentMillis); print(" "); printcolourArray(); print(" ");
        println("main.ino Wrong sequence - too few colours");
        break;
    default:
        {}
    }

    turning();
   // cleancolourArray(); // sposob przekazywania informacji odnosnie ktora akcje wykonac

}

