#ifndef MOTORCONTROL_H_INCLUDED
#define MOTORCONTROL_H_INCLUDED


#define println Serial.println
#define print Serial.print

#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"

void velocityHandler(int changeInVelocity) {
    int temp = velocityThroughout;
    temp += changeInVelocity;
    if (temp < velocityMinNumber) {
        temp = velocityMinNumber;
        print("motorControl.h: Chciales za bardzo zwolnic\n");
    } else if (temp > velocityMaxNumber) {
        temp = velocityMaxNumber;
        print("motorControl.h: Chciales za bardzo przyspieszyc\n");
    }
    velocityThroughout = temp;
    print("motorControl.h: predkosc zmieniona na: ");print(velocityThroughout);print("\n");
}

void velocityHandlerColours() {
    if (velocityThroughout != velocityOnColours) {
        velocityThroughout = velocityOnColours;
        print("motorControl.h: predkosc zmieniona na: ");print(velocityThroughout);print("\n");
    }
}

uint8_t velocityThroughoutToPWM(const int &velocityThroughout) {
    switch(velocityThroughout) {
    case -2:
        return 20;
    case - 1:
        return 40;
    case 0:
        return 60;
    case 1:
        return 80;
    case 2:
        return 100;
    default:
        println("velocityThroughoutToPWM.h: Shouldn't have happended");
        return 0;
    }
};

template <uint8_t PWM_PIN, uint8_t CONTROL_PIN1, uint8_t CONTROL_PIN2>
void motorControl(uint8_t PwmDuty, uint8_t in1, uint8_t in2) {
    digitalWrite(CONTROL_PIN1, in1);
    digitalWrite(CONTROL_PIN2, in2);
    analogWrite(PWM_PIN, PwmDuty);
}
/*
void motorControl(bool ifDoPrzodu, int procentVelocity, motor motorChoice) {
    //wyskaluj pr�dko��
    int velocity = map(abs(procentVelocity), 0, 100, 0, currentPWM);
    uint8_t chosenMotorDirection = 0;
    uint8_t chosenMotorPWM = 0;
    // wybranie silnika
    if (motorChoice == 0){
        chosenMotorDirection = LeftEngineDirection;
        chosenMotorPWM = LeftEnginePWM;
    } else {
        chosenMotorDirection = RightEngineDirection;
        chosenMotorPWM = RightEnginePWM;
    }
    // je�li do przodu zero na pin steruj�cy wybranego silnika, je�li nie jeden
    ifDoPrzodu ? digitalWrite(chosenMotorDirection, 0) : digitalWrite(chosenMotorDirection, 1);
    //zapu�� odpowiedni PWM na wybrany Pin
    analogWrite(chosenMotorPWM, velocity);
}
*/


#endif // MOTORCONTROL_H_INCLUDED
