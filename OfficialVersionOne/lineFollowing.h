#ifndef LINEFOLLOWING_H_INCLUDED
#define LINEFOLLOWING_H_INCLUDED

#define println Serial.println
#define print Serial.print

#include "variables.h"
#include "actions.h"
#include "colourReading.h"
#include "turning.h"
#include "lineFollowing.h"
#include "motorControl.h"
#include "dataFromFile.h"

//==============sensor====================================//
//void setSensor() {
//    qtr.setTypeAnalog();
  //  qtr.setSensorPins((const uint8_t[]){SENSOR_ARRAY_1_PIN, SENSOR_ARRAY_2_PIN, SENSOR_ARRAY_3_PIN, SENSOR_ARRAY_4_PIN}, sensor Count);
//
   // qtr.calibrate();
 //   qtr.setEmitterPin(SENSOR_ARRAY_EMMITER_CONTROL_PIN);
//}

void sensorArrayCallibration() {
    //przy kalibracji zakladamy ze ktorys z dwoch srodkowych stoi nad linia ???
    int bialy, czarny;
    sensorArray.read(sensorValues);
    bialy = sensorValues[0];
    if (sensorValues[1] > sensorValues[2]) {
        czarny = sensorValues[1];
    } else {
        czarny = sensorValues[2];
    }
    threshold = bialy + (czarny-bialy)/2;
}

void readADC() {
    sensorArray.read(sensorValues);
    for (int i = 0; i<4; i++) {
        if(sensorValues[i] > threshold) {
            sensorBoolResults[i] = 1;
        } else {
            sensorBoolResults[i] = 0;
        }
    }
}

int calculateError() {
    int err = 0;
    int amountWithLine = 0;

    for (int i = 0; i<4; i++) {
        err += sensorBoolResults[i] * weights[i];
        amountWithLine += sensorBoolResults[i];
    }
    if (amountWithLine != 0) { //czujniki wykryly linie, liczymy blad
        err /= amountWithLine;
        previousError = err;
    } else {
        err = previousError;
    }

    return err;
}

int PD() {
    int derivative = error - madeError;
    madeError = error;
    return Kp * error + Kd * derivative;
}

void lineFollowing() {
    readADC();
    error = calculateError();
    int regulation = PD();


}
//============= funkcje linefollowerowe ==================//
/*
bool leftSensor() { //czy lewy widzi linie
    long sensorInput = random(0,1023);
    //Serial.print("Wartoœæ lewego czujnika : ");
    //Serial.println(sensorInput);
    if (sensorInput > granica) {
        return true;
    } else {
        return false;
    }
}

bool rightSensor() { //czy prawy widzi linie
    long sensorInput = random(0,1023);
    //Serial.print("Wartoœæ prawego czujnika : ");
    //Serial.println(sensorInput);
    if (sensorInput > granica) {
        return true;
    } else {
        return false;
    }
}

void followingLineCheck() {// kontroluje wyjscia do silnikow w zaleznosci od wartosci czujnikow
    // oba czujniki widzą linię
    //do przodu na pełnej (w sumie to nie na pełnej)
    if (leftSensor() && rightSensor()) {
        motorControl(1,40,Left);
        motorControl(1,40, Right);
    } else if (!leftSensor()) {
        motorControl(1,40,Left);
        motorControl(1,10,Right);
    } else if (!leftSensor()) {
        motorControl(1,10,Left);
        motorControl(1,40,Right);
    }
}
*/
#endif // LINEFOLLOWING_H_INCLUDED
